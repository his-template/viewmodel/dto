export class HisResponse<T>{

    code: string;
    data: Array<T>
    message: string

    constructor(
        code: string,
        data: Array<T>,
        message: string) {

        this.code = code;
        this.data = data;
        this.message = message;

    }
}